#include <iostream>
#include "students.pb.h"


void print(const university::StudentsGroup& sg)
{
    for(int i{0}; i < sg.student_size(); ++i)
    {
        std::cout << "Name: " << sg.student(i).fullname().name() << std::endl;
        std::cout << "Surname: " << sg.student(i).fullname().surname() << std::endl;
        std::cout << "Patronymic: " << sg.student(i).fullname().patronymic() << std::endl;

        std::cout << "Grades: ";
        for(int j{0}; j < sg.student(i).grades_size(); ++j)
        {
            std::cout << sg.student(i).grades(j) << " ";
        }

        std::cout << "GPA: " << sg.student(i).gpa();
    }
}


double gpa(const university::Student& s)
{
    double result{0.0};

    for(int i{0}; i < s.grades_size(); ++i)
    {
        result += s.grades(i);
    }

    return (result / s.grades_size());
}


int main()
{
    university::StudentsGroup sgroup;
    sgroup.add_student();
    sgroup.mutable_student(0)->mutable_fullname()->set_name("Petr");
    sgroup.mutable_student(0)->mutable_fullname()->set_surname("Ivanov");
    sgroup.mutable_student(0)->mutable_fullname()->set_patronymic("");
    sgroup.mutable_student(0)->add_grades(5);
    sgroup.mutable_student(0)->add_grades(3);
    sgroup.mutable_student(0)->add_grades(4);
    sgroup.mutable_student(0)->set_gpa(gpa(sgroup.student(0)));

    sgroup.add_student();
    sgroup.mutable_student(1)->mutable_fullname()->set_name("Ivan");
    sgroup.mutable_student(1)->mutable_fullname()->set_surname("Petrov");
    sgroup.mutable_student(0)->mutable_fullname()->set_patronymic("");
    sgroup.mutable_student(1)->add_grades(3);
    sgroup.mutable_student(1)->add_grades(2);
    sgroup.mutable_student(1)->add_grades(4);
    sgroup.mutable_student(1)->set_gpa(gpa(sgroup.student(1)));

    print(sgroup);

    return 0;
}
