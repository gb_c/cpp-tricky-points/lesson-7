#ifndef IMETHODS_H
#define IMETHODS_H

#include <string>
#include "students.pb.h"


class IMethods
{
    virtual double GetAverageScore(const university::FullName& name) = 0;
    virtual std::string GetAllInfo(const university::FullName& name) = 0;
    virtual std::string GetAllInfo() = 0;
};

#endif // IMETHODS_H
