cmake_minimum_required(VERSION 3.14)
project(task_3)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

include(FindProtobuf)
find_package(Protobuf REQUIRED)

set(SOURCE_FILES
    main.cpp
    irepository.h
    imethods.h
    studentsgroup.cpp
    studentsgroup.h
    proto/students.proto
)

protobuf_generate_cpp(PROTO_SRC PROTO_HEADER proto/students.proto)
add_library(proto ${PROTO_HEADER} ${PROTO_SRC})
add_executable(${PROJECT_NAME} ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} proto ${PROTOBUF_LIBRARY})

target_include_directories(${PROJECT_NAME} PRIVATE ${PROJECT_BINARY_DIR})
