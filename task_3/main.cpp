#include "studentsgroup.h"


double gpa(const university::Student& s)
{
    double result{0.0};

    for(int i{0}; i < s.grades_size(); ++i)
    {
        result += s.grades(i);
    }

    return (result / s.grades_size());
}


int main()
{
    university::StudentsGroup sgroup;
    sgroup.add_student();
    sgroup.mutable_student(0)->mutable_fullname()->set_name("Petr");
    sgroup.mutable_student(0)->mutable_fullname()->set_surname("Ivanov");
    sgroup.mutable_student(0)->mutable_fullname()->set_patronymic("Sergeevich");
    sgroup.mutable_student(0)->add_grades(5);
    sgroup.mutable_student(0)->add_grades(3);
    sgroup.mutable_student(0)->add_grades(4);
    sgroup.mutable_student(0)->set_gpa(gpa(sgroup.student(0)));

    sgroup.add_student();
    sgroup.mutable_student(1)->mutable_fullname()->set_name("Ivan");
    sgroup.mutable_student(1)->mutable_fullname()->set_surname("Petrov");
    sgroup.mutable_student(1)->add_grades(3);
    sgroup.mutable_student(1)->add_grades(2);
    sgroup.mutable_student(1)->add_grades(4);
    sgroup.mutable_student(1)->set_gpa(gpa(sgroup.student(1)));

    StudentsGroup sg(sgroup);
    sg.Save();
    sg.Open();

    university::FullName fn;
    fn.set_name("Ivan");
    fn.set_surname("Petrov");

    sg.GetAverageScore(fn);
    sg.GetAllInfo(fn);
    sg.GetAllInfo();

    return 0;
}
