#ifndef IREPOSITORY_H
#define IREPOSITORY_H


class IRepository
{
    virtual void Open() = 0;    // бинарная десериализация в файл
    virtual void Save() = 0;    // бинарная сериализация в файл
};

#endif // IREPOSITORY_H
