#include "studentsgroup.h"


StudentsGroup::StudentsGroup(const university::StudentsGroup &sg)
    : _sg(sg)
{}


void StudentsGroup::Open()
{
    std::ifstream in("data.bin", std::ios_base::binary);

    if(!_sg.ParseFromIstream(&in))
    {
        std::cout << "Error open!" << std::endl;
    }
}


void StudentsGroup::Save()
{
    std::ofstream out("data.bin", std::ios_base::binary);

    if(!_sg.SerializeToOstream(&out))
    {
        std::cout << "Error save!" << std::endl;
    }
}


double StudentsGroup::GetAverageScore(const university::FullName& name)
{
    double result{0.0};

    for(int i{0}; i < _sg.student_size(); ++i)
    {
        university::FullName iName = _sg.student(i).fullname();

        if(std::tie(iName.name(), iName.surname(), iName.patronymic()) ==
           std::tie(name.name(), name.surname(), name.patronymic()))
        {
            result = _sg.student(i).gpa();
            break;
        }
    }

    return result;
}


std::string StudentsGroup::GetAllInfo(const university::FullName& name)
{
    std::string result;

    for(int i{0}; i < _sg.student_size(); ++i)
    {
        university::FullName iName = _sg.student(i).fullname();

        if(std::tie(iName.name(), iName.surname(), iName.patronymic()) ==
           std::tie(name.name(), name.surname(), name.patronymic()))
        {
            result.append("Name: " + _sg.student(i).fullname().name() + '\n');
            result.append("Surname: " + _sg.student(i).fullname().surname() + '\n');
            result.append("Patronymic: " + _sg.student(i).fullname().patronymic() + '\n');

            result.append("Grades: ");
            for(int j{0}; j < _sg.student(i).grades_size(); ++j)
            {
                result.append(std::to_string(_sg.student(i).grades(j)) + " ");
            }

            result.append("\nGPA: " + std::to_string(_sg.student(i).gpa()) + '\n');
            break;
        }
    }

    return result;
}


std::string StudentsGroup::GetAllInfo()
{
    std::string result;

    for(int i{0}; i < _sg.student_size(); ++i)
    {
        result.append("Name: " + _sg.student(i).fullname().name() + '\n');
        result.append("Surname: " + _sg.student(i).fullname().surname() + '\n');
        result.append("Patronymic: " + _sg.student(i).fullname().patronymic() + '\n');

        result.append("Grades: ");
        for(int j{0}; j < _sg.student(i).grades_size(); ++j)
        {
            result.append(std::to_string(_sg.student(i).grades(j)) + " ");
        }

        result.append("\nGPA: " + std::to_string(_sg.student(i).gpa()) + "\n\n");
    }

    return result;
}
