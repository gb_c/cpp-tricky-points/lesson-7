#ifndef STUDENTSGROUP_H
#define STUDENTSGROUP_H

#include <iostream>
#include <fstream>
#include <tuple>
#include "imethods.h"
#include "irepository.h"


class StudentsGroup : public IMethods, public IRepository
{
private:
    university::StudentsGroup _sg;

public:
    StudentsGroup(const university::StudentsGroup& sg);

    void Open() override;
    void Save() override;

    double GetAverageScore(const university::FullName& name) override;
    std::string GetAllInfo(const university::FullName& name) override;
    std::string GetAllInfo() override;
};

#endif // STUDENTSGROUP_H
